<?php
// dinhvt
function date_sort($a, $b)
{
    return strtotime($b[1]) - strtotime($a[1]);
}

function get_actual_activation_date($data)
{
    $array_len = count($data) - 1;

    for ($i = 0; $i < $array_len - 1; $i++) {
        $current_activation_date = $data[$i][1];
        $next_deactivation_date = $data[$i + 1][2];
        if ($current_activation_date != $next_deactivation_date)
            return $current_activation_date;
    }
    return $data[$array_len][1];
    
}

function export_to_csv($data) {
    $fp = fopen('output.csv', 'w');
    fputcsv($fp, ['PHONE_NUMBER','REAL_ACTIVATION_DATE']);
    foreach ($data as $key => $fields) {
        fputcsv($fp, [$key,$fields]);
    }
}

function main() {
    $data = array_map('str_getcsv', file('data.csv'));

    $header = array_shift($data);
    $arr = array();
    foreach ($data as $item) {
        $arr[$item[0]][] = $item;
    }

    //sort date and get result
    $result = [];
    foreach ($arr as $key => $item) {
        usort($arr[$key], "date_sort");
        $result[$key] = get_actual_activation_date($arr[$key]);
    }

    export_to_csv($result);
    echo "---done---";
}


main();